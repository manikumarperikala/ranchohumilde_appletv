//
//  CollectionContentCellTableViewCell.swift
//  Rancho Humilde
//
//  Created by Sanchan on 16/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import SystemConfiguration
import Kingfisher

class CollectionContentCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    var menuCollectionList = NSMutableArray()
    var UserId,DeviceId,uuid : String!
    var CarousalDict = [[String:Any]]()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuCollectionList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCell", for: indexPath)
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        let metaDataPath = Path[kMetadata] as! NSDictionary
        if metaDataPath[kIsmenu] != nil
        {
            if metaDataPath[kIsmenu] as! Bool == true
            {
                (cell.viewWithTag(11) as! UIImageView).image = metaDataPath[kMovieart] as? UIImage
            }
        }
        else
        {
            let img = (cell.viewWithTag(11) as! UIImageView)
            img.kf.indicatorType = .activity
            (cell.viewWithTag(11) as! UIImageView).kf.setImage(with: URL(string: metaDataPath[kMovieart] as! String))
        }
        collectionView.isScrollEnabled = true
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        let tableview:UITableView = self.superview?.superview as! UITableView
        //Previous Index Focus
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            cell.transform = .identity
        }
        
        //Next Index Focus
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
            (cell.viewWithTag(11) as! UIImageView).adjustsImageWhenAncestorFocused = true
            let viewcontroller = tableview.dataSource as! MainViewController
            let Path = menuCollectionList[indexPath.row] as! NSDictionary
            let image = Path[kMetadata] as! NSDictionary
            if image[kCarouselId] as! String == "Menu"
            {
                if indexPath.row == 0
                {
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "categories_shelf")
                    viewcontroller.staticLbl.text = "Categories"
                    viewcontroller.lbl2.text = ""
                }
                if indexPath.row == 1
                {
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "settings_Shelf")
                    viewcontroller.staticLbl.text = "Settings"
                    viewcontroller.lbl2.text = "Your account information"
                }
                if indexPath.row == 2
                {
                    viewcontroller.MainImage.kf.indicatorType = .activity
                    viewcontroller.MainImage.image = UIImage(imageLiteralResourceName: "Search_shelf")
                    viewcontroller.staticLbl.text = "Search"
                    viewcontroller.lbl2.text = "Search for TV shows,movies"
                }
                viewcontroller.AssestName.text = ""
                viewcontroller.ReleaseDate.text = ""
                viewcontroller.TimeLbl.text = ""
                viewcontroller.Description.text = ""
                (viewcontroller.view.viewWithTag(1))?.isHidden = true
            }
            else
            {
                viewcontroller.staticLbl.text = ""
                viewcontroller.lbl2.text = ""
                viewcontroller.MainImage.kf.indicatorType = .activity
                viewcontroller.MainImage.kf.setImage(with: URL(string: image["main_carousel_image_url"] as! String))
                viewcontroller.AssestName.text = (Path["name"] as? String)?.capitalized
                let str1 = (image["release_date"] as! String).components(separatedBy: "-")
                viewcontroller.ReleaseDate.text = str1[0]
                viewcontroller.TimeLbl.text =  stringFromTimeInterval(interval: Double((Path["file_duration"] as! String))!) as String
                let DescriptionText = Path["description"] as? String
                let destxt = DescriptionText?.replacingOccurrences(of: "&", with: "", options: .literal, range: nil)
                let destxt1 =  destxt?.replacingOccurrences(of: "amp", with: "", options: .literal, range: nil)
                let destxt2 = destxt1?.replacingOccurrences(of: ";", with: "", options: .literal, range: nil)
                
                viewcontroller.Description.text = destxt2
                if (DescriptionText?.contains("n/a"))!
                {
                    viewcontroller.Description.text = ""
                }
                let labelText = (Path["name"] as? String)?.capitalized
                let desLbl = viewcontroller.Description
                let lbl = viewcontroller.AssestName
                lbl?.frame = CGRect(x: (lbl?.frame.origin.x)!, y: (lbl?.frame.origin.y)!, width: (labelText?.widthWithConstrainedWidth(height: 60, font: (lbl?.font)!))!, height: (lbl?.frame.size.height)!)
                desLbl?.frame = CGRect(x: (desLbl?.frame.origin.x)!, y: (desLbl?.frame.origin.y)!, width: (desLbl?.frame.size.width)!, height: (DescriptionText?.widthWithConstrainedWidth(height: 60, font: (desLbl?.font)!))!)
                (viewcontroller.view.viewWithTag(1))?.isHidden = false
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Path = menuCollectionList[indexPath.row] as! NSDictionary
        let MetaDict = Path[kMetadata] as! NSDictionary
        if ((MetaDict[kCarouselId] as! String) == "Menu")
        {
            if indexPath.row == 0
            {
                gotoCategories()
            }
            if indexPath.row == 1
            {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let SettingsPage = storyBoard.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                SettingsPage.deviceId = viewcontroller.deviceId
                SettingsPage.uuid = viewcontroller.uuid
                viewcontroller.navigationController?.pushViewController(SettingsPage, animated: true)
            }
            if indexPath.row == 2
            {
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                guard let searchResultsController = storyboard.instantiateViewController(withIdentifier: "Search") as? SearchListViewController
                    else {
                        fatalError("Unable to instatiate a SearchResultsViewController from the storyboard.")
                }
                let searchController = UISearchController(searchResultsController: searchResultsController)
                searchController.searchResultsUpdater = searchResultsController
                searchController.view.backgroundColor = UIColor.init(red: 13/255, green: 13/255, blue: 13/255, alpha: 1)
                searchController.searchBar.setScopeBarButtonTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .normal)
                searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.dark
                searchController.searchBar.placeholder = NSLocalizedString("Enter keyword (e.g. The Secret to Ballin)", comment: "")
                let searchContainer = UISearchContainerViewController(searchController: searchController)
                searchContainer.title = NSLocalizedString("Search", comment: "")
                searchResultsController.searchCollectionList = viewcontroller.searchCollectionList
                searchResultsController.userId = viewcontroller.userId
                searchResultsController.deviceId = viewcontroller.deviceId
                searchResultsController.uuid = viewcontroller.uuid
                searchResultsController.searchDelegate = viewcontroller
                viewcontroller.navigationController?.pushViewController(searchContainer, animated: false)
            }
        }
        else
        {
            getaccountInfo(id:Path["id"] as! String,userid: UserId/*,tvshow: (Path["tv_show"] as! Bool)*/)
            // getAssetData(withUrl:kAssestDataUrl,id: Path["id"] as! String,userid: UserId,tvshow:(Path["tv_show"] as! Bool),accountstatus:accountInfoStatus)
        }
    }
    
    func carousalSelected(carousalName:String,carousalDetailDict:[[String:Any]],userid:String,deviceid:String,uuid:String)
    {
        self.menuCollectionList.removeAllObjects()
        self.UserId = userid
        self.DeviceId = deviceid
        self.CarousalDict = carousalDetailDict
        self.uuid = uuid
        for dict in carousalDetailDict
        {
            if carousalName == "Menu"
            {
                self.menuCollectionList.add(dict)
            }
            else
            {
                if carousalName == "My List"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                    }
                }
                else if carousalName == "Recently Watched"
                {
                    if dict[kData] != nil
                    {
                        let dicton = dict[kData] as! NSDictionary
                        self.menuCollectionList.add(dicton)
                    }
                    else
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
                else
                {
                    let metaDict = dict[kMetadata] as! NSDictionary
                    if (metaDict[kCarouselId] as! String) == carousalName
                    {
                        self.menuCollectionList.add(dict)
                    }
                }
            }
        }
        DispatchQueue.main.async {
            self.menuCollectionView.isHidden = false
            self.menuCollectionView.reloadData()
            
        }
    }
    
    // Service Call for getAssetData
    func getAssetData(withUrl:String,id:String,userid:String,/*tvshow:Bool,*/subscription_status:String)
    {
        //        if accountstatus == true
        //        {
        var parameters =  [String:[String:AnyObject]]()
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let osVersion = UIDevice.current.systemVersion
        let className = NSStringFromClass(self.classForCoder)
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        RHApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
                let DetailPage = storyBoard.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    DetailPage.TvshowPath = dict as! NSDictionary
                }
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.DeviceId
                DetailPage.uuid = self.uuid
                let tableview:UITableView = self.superview?.superview as! UITableView
                let viewcontroller = tableview.dataSource as! MainViewController
                DetailPage.delegate = viewcontroller
                DetailPage.isMain = true
                viewcontroller.navigationController?.pushViewController(DetailPage, animated: true)
            }
            else
            {
                let listError = (error?.localizedDescription)! as String
                UserDefaults.standard.set(listError, forKey: "locerror")
                UserDefaults.standard.synchronize()
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    viewcontroller.navigationController?.popToRootViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                // viewcontroller.navigationController?.pushViewController(alertview, animated: true)
                viewcontroller.present(alertview, animated: true, completion: nil)
                parameters =  ["insertLog":["source":("\(withUrl) - \(className) - \(#function)") as AnyObject,"error":(listError) as AnyObject,"code":parameters as AnyObject,"details":("userId:\(userid) - AppVersion:\(appVersionString) - OSVersion:\(osVersion)") as AnyObject,"device":"AppleTV" as AnyObject]]
                RHApiManager.sharedManager.postDataWithJson(url: kLogUrl, parameters: parameters)
                {(responseDict,error,isDone)in
                    if error == nil
                    {
                        _ = responseDict
                    }
                }
            }
        }
    }
    
    func getaccountInfo(id:String,userid:String/*tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":DeviceId as AnyObject,"uuid":uuid as AnyObject]]
        RHApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse == true
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id: id,userid: userid,/*tvshow:tvshow,*/subscription_status:(dict.value(forKey: "subscription_status") as! String))
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
        
    }
    
    // Categories Controller
    func gotoCategories()
    {
        let storyBoard = UIStoryboard(name: kBoardname, bundle: nil)
        let CategoriesPage = storyBoard.instantiateViewController(withIdentifier: "Categories") as! CategoriesViewController
        let tableview:UITableView = self.superview?.superview as! UITableView
        let viewcontroller = tableview.dataSource as! MainViewController
        CategoriesPage.CategoryCarousalName = viewcontroller.CategoryCarousalName
        CategoriesPage.CarousalData = viewcontroller.CarousalData
        CategoriesPage.UserId = viewcontroller.userId
        CategoriesPage.uuid = viewcontroller.uuid
        CategoriesPage.deviceId = viewcontroller.deviceId
        CategoriesPage.MyListthumb = viewcontroller.myList
        CategoriesPage.categoryDelegate = viewcontroller
        viewcontroller.navigationController?.pushViewController(CategoriesPage, animated: true)
    }
    
}
