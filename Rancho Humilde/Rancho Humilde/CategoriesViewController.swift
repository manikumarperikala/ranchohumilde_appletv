//
//  CategoriesViewController.swift
//  Rancho Humilde
//
//  Created by Sanchan on 16/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    var CarousalData = [[String:Any]]()
    var CategoryCarousalName = NSMutableArray()
    var menuCollectionList = NSMutableArray()
    var UserId,uuid,deviceId:String!
    var MyListthumb = [[String:Any]]()
    var categoryDelegate:myListdelegate!
    var insetButton : UIButton = UIButton()
    private  var rightHandFocusGuide = UIFocusGuide()
    
    @IBOutlet weak var listcollectionview: UICollectionView!
    @IBOutlet weak var backgroundimg: UIImageView!
    @IBOutlet weak var focusButton: UIButton!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieTime: UILabel!
    @IBOutlet weak var rightHandView:UIView!
    @IBOutlet weak var tableview: UITableView!
    
    var viewToFocus: UIView? = nil
        {
        didSet
        {
            if viewToFocus != nil
            {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    override weak var preferredFocusedView: UIView?
    {
        if viewToFocus != nil
        {
            return viewToFocus;
        }
        else
        {
            return super.preferredFocusedView
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        insetButton.frame = CGRect(x: 0, y: 630, width: 10, height: 10)
        insetButton.backgroundColor = UIColor.clear
        insetButton.layer.borderWidth = 1.0
        insetButton.layer.borderColor = UIColor.black.cgColor
        insetButton.setTitle("Press This", for: .normal)
        insetButton.setTitle("Highlighted This", for: .highlighted)
        insetButton.setTitle("Selected This", for: .selected)
        insetButton.setTitle("Focused This", for: .focused)
        insetButton.setTitleColor(UIColor.black, for: .normal)
        rightHandView.addSubview(insetButton)
        view.addLayoutGuide(rightHandFocusGuide)
        
        rightHandFocusGuide.bottomAnchor.constraint(equalTo: tableview.bottomAnchor).isActive = true
        rightHandFocusGuide.leftAnchor.constraint(equalTo: tableview.rightAnchor).isActive = true
        rightHandFocusGuide.topAnchor.constraint(equalTo: tableview.topAnchor).isActive = true
        rightHandFocusGuide.rightAnchor.constraint(equalTo: rightHandView.leftAnchor).isActive = true
        rightHandFocusGuide.preferredFocusedView = listcollectionview
        textView.isHidden = true
        textView.isUserInteractionEnabled = false
    }
    
    //Number of section
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    //Number of rows in a Section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return CategoryCarousalName.count
    }
    
    //Cell Item at IndexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = CategoryCarousalName[indexPath.row] as? String
        cell.textLabel?.font = UIFont(name:"Avenir-Light", size: 28.0)
        cell.textLabel?.textColor = UIColor.white
        cell.selectionStyle = .none
        cell.layer.cornerRadius = 8.0
        return cell
    }
    
    //Did Update Focus
    func tableView(_ tableView: UITableView, didUpdateFocusIn context: UITableViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        guard let nextView = context.nextFocusedView else { return }
        if (nextView == insetButton)
        {
            guard let indexPath = context.previouslyFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath) else {
                return
            }
            cell.textLabel?.textColor = UIColor.white
            rightHandFocusGuide.preferredFocusedView = cell
        }
        else
        {
            rightHandFocusGuide.preferredFocusedView = insetButton
        }
        guard let indexPath = context.nextFocusedIndexPath, let cell = tableView.cellForRow(at: indexPath)
            else { return }
        cell.textLabel?.textColor = UIColor.black
        let selectedCarousal = self.CategoryCarousalName[(indexPath as NSIndexPath).row] as! String
        categoriesSelected(carousalName: selectedCarousal,carousalDetailDict: CarousalData)
        guard let prevIndexPath = context.previouslyFocusedIndexPath, let prevCell = tableView.cellForRow(at: prevIndexPath)
            else { return }
        prevCell.textLabel?.textColor = UIColor.white
    }
    
    // Number of section in listcollectionview
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    // Number of rows in listcollectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuCollectionList.count
    }
    
    //Cell for Item in listcollectionview
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell1" , for: indexPath)
        let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
        let image = imagePath["metadata"] as! NSDictionary
        let img = (cell.viewWithTag(10) as! UIImageView)
        img.kf.indicatorType = .activity
        (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: image["movie_art"] as! String))
        switch indexPath.row
        {
        case 0:
            textView.isHidden = false
            backgroundimg.kf.setImage(with: URL(string: image["main_carousel_image_url"] as! String))
            movieTitle.text = (imagePath["name"] as? String)?.capitalized
            movieTitle?.frame = CGRect(x: (movieTitle?.frame.origin.x)!, y: (movieTitle?.frame.origin.y)!, width: ((movieTitle.text)?.widthWithConstrainedWidth(height: 60, font: (movieTitle?.font)!))!, height: (movieTitle?.frame.size.height)!)
            let str1 = (image["release_date"] as! String).components(separatedBy: "-")
            movieYear.text = str1[0]
            movieTime.text =  stringFromTimeInterval(interval: Double((imagePath["file_duration"] as! String))!) as String
            break
        default:
            break
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    // listcollectio view update focus
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        textView.isHidden = false
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            let imagePath = menuCollectionList[previousIndexPath.row] as! NSDictionary
            let image = imagePath["metadata"] as! NSDictionary
            backgroundimg.kf.setImage(with: URL(string: image["main_carousel_image_url"] as! String))
            cell.transform = CGAffineTransform.identity
        }
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath)
        {
            let imagePath = menuCollectionList[indexPath.row] as! NSDictionary
            let image = imagePath["metadata"] as! NSDictionary
            backgroundimg.kf.setImage(with: URL(string: image["main_carousel_image_url"] as! String))
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
            (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
            movieTitle.text = (imagePath["name"] as? String)?.capitalized
            movieTitle?.frame = CGRect(x: (movieTitle?.frame.origin.x)!, y: (movieTitle?.frame.origin.y)!, width: ((movieTitle.text)?.widthWithConstrainedWidth(height: 60, font: (movieTitle?.font)!))!, height: (movieTitle?.frame.size.height)!)
            let str1 = (image["release_date"] as! String).components(separatedBy: "-")
            movieYear.text = str1[0]
            movieTime.text =  stringFromTimeInterval(interval: Double((imagePath["file_duration"] as! String))!) as String
        }
    }
    
    // did select list collection view
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let path = menuCollectionList[indexPath.row] as! NSDictionary
        getaccountInfo(id:path["id"] as! String,userid:UserId/*,tvshow:(path["tv_show"] as! Bool)*/)
        //  getAssetData(withUrl:kAssestDataUrl,id: path["id"] as! String,userid: UserId,tvshow:(path["tv_show"] as! Bool))
    }
    
    // Service call of get assetdata
    func getAssetData(withUrl:String,id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAssestData":["videoId":id as AnyObject,"userId":userid as AnyObject,"returnType":"tiny" as AnyObject]]
        RHApiManager.sharedManager.postDataWithJson(url: withUrl, parameters: parameters){(responseDict , error,isDone) in
            if error == nil
            {
                let JSON = responseDict
                let DetailPage = self.storyboard?.instantiateViewController(withIdentifier: "DetailPage") as! DetailPageViewController
                let jsonresponse = JSON as! NSArray
                for dict in jsonresponse
                {
                    DetailPage.TvshowPath = dict as! NSDictionary
                }
                DetailPage.delegate = self.categoryDelegate
                DetailPage.userid = self.UserId
                DetailPage.deviceId = self.deviceId
                DetailPage.uuid = self.uuid
                self.navigationController?.pushViewController(DetailPage, animated: true)
                //   self.present(DetailPage, animated: true, completion: nil)
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    func getaccountInfo(id:String,userid:String/*,tvshow:Bool*/)
    {
        var parameters =  [String:[String:AnyObject]]()
        parameters = ["getAccountInfo":["deviceId":deviceId as AnyObject,"uuid":uuid as AnyObject]]
        RHApiManager.sharedManager.postDataWithJson(url: kAccountInfoUrl, parameters: parameters){
            (responseDict,error,isDone) in
            if error == nil
            {
                let Json = responseDict
                let dict = Json as! NSDictionary
                accountresponse = (dict.value(forKey: "uuid_exist") as! Bool)
                if accountresponse
                {
                    self.getAssetData(withUrl:kAssestDataUrl,id:id,userid: userid/*,tvshow:tvshow*/)
                }
                else
                {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.gotoCode()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func myListdata(mylistDict: NSDictionary)
    {
    }
    func removeListdata(id : String)
    {
        
    }
    func recentlyWatcheddata(recentlyWatchedDict: NSDictionary) {
        
    }
}

extension CategoriesViewController
{
    internal func categoriesSelected(carousalName: String, carousalDetailDict: [[String : Any]]) {
        
        self.menuCollectionList.removeAllObjects()
        let carousal = carousalName
        for dict in carousalDetailDict
        {
            let metaDict = dict["metadata"] as! NSDictionary
            if (metaDict["carousel_id"] as! String) == carousal
            {
                self.menuCollectionList.add(dict)
            }
        }
        self.listcollectionview.isHidden = false
        self.listcollectionview.reloadData()
    }
}
