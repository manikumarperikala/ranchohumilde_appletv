//
//  PlayerViewController.swift
//  Rancho Humilde
//
//  Created by Sanchan on 16/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import AVKit

protocol playerdelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}

class PlayerViewController: AVPlayerViewController, AVPlayerViewControllerDelegate {
    
    var videoUrl,userID,videoID,mainVideoID : String!
    var seektime = Float64()
    var updatetime = Float64()
    var isResume = Bool()
    var detdelegate:playerdelegate?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // playvideo Implementation
    func playVideo(userId:String,videoId:String) {
        let playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        userID = userId
        videoID = videoId
        player = AVPlayer(playerItem: playerItem)
        if isResume
        {
            var sTime = Float64()
            if (UserDefaults.standard.object(forKey: "seektime") != nil)
            {
                sTime = UserDefaults.standard.object(forKey: "seektime") as! Float64
            }
            let targetTime = CMTime(seconds: sTime, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player?.seek(to:targetTime , toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player?.play()
            
        }
        else
        {
            player?.play()
        }
        
        player?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                if self.player?.currentItem?.status == .readyToPlay
                {
                    self.seektime = CMTimeGetSeconds(self.player!.currentTime())
                }
        })
        
        Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.UpdateseekTime), userInfo: nil, repeats: true)
    }
    
    // seektime
    func UpdateseekTime()
    {
        let parameters = ["updateSeekTime":["userId": userID as AnyObject, "videoId": (videoID) as AnyObject, "seekTime": self.seektime]]
        RHApiManager.sharedManager.postDataWithJson(url: kUpdateseekUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["watchedVideo"] != nil
                {
                    self.updatetime = Float64((((JSON)["watchedVideo"] as! NSDictionary)["seekTime"]) as! Float64)
                    UserDefaults.standard.set(self.updatetime, forKey: "seektime")
                    UserDefaults.standard.synchronize()
                    if self.updatetime > 0
                    {
                        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
                    }
                }
            }
            else
            {
                print("json error")
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                    self.navigationController?.popViewController(animated: true)
                })
                alertview.addAction(defaultAction)
                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
}
